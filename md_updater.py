import logging
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s\t%(levelname)s" +
    "\t%(module)s:%(funcName)s:%(lineno)d" +
    "\t%(message)s")

from watchdog import events

import sh


class Notifier(events.FileSystemEventHandler):
    def __init__(self, md_file_name, bib_file=None, csl_file=None):
        super(Notifier, self).__init__()

        self.file_name = md_file_name

        fn_list = self.file_name.split('.')[:-1]
        fn_list.append('pdf')

        self._kwargs = {}

        self._kwargs['o'] = '.'.join(fn_list)

        if bib_file is not None:
            self._kwargs['bibliography'] = bib_file

        if csl_file is not None:
            self._kwargs['csl'] = csl_file

    def on_modified(self, event):
        logging.info("Event noticed: %s" % event)
        is_md_file = event.src_path.endswith(self.file_name)
        try:
            is_bib_file = event.src_path.endswith(self._kwargs['bibliography'])
        except KeyError:
            is_bib_file = False
        if is_md_file or is_bib_file:
            logging.info("Updating %s with %s",
                         self.file_name, self._kwargs)
            sh.pandoc(self.file_name, **self._kwargs)

if __name__ == '__main__':
    import sys
    import time
    from watchdog import observers
    o = observers.Observer()
    n = Notifier(sys.argv[1])
    o.schedule(n, path='.', recursive=True)
    o.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print "Stopping..."
    finally:
        o.stop()
        o.join()
        print "Stopped"
